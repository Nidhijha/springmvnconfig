package com.ust.boot;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.ust.config.SpringConfig;
import com.ust.spring.Employee;

public class SpringBootMain {

	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);
		Employee emp1 = (Employee) ctx.getBean("emp");
		System.out.println(emp1);
		emp1.getSkills();
	}

}
