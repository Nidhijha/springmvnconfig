package com.ust.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfigList {

@Bean(name="skill")
	public List<String> getSkill(){
		List<String> skillLest = new ArrayList<>();
		skillLest.add("Java");
		skillLest.add("Python");
		skillLest.add("Sql");
		skillLest.add("Spring");
		return skillLest;
		}
}
