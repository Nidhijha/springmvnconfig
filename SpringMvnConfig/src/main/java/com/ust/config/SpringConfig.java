package com.ust.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;

import com.ust.spring.Address;
import com.ust.spring.Employee;

@Configuration
@Import(value = {SpringConfigList.class})
public class SpringConfig {

	@Bean(name="emp")
	@Scope(scopeName = "prototype")
	public Employee getEmpBean() {
		Employee e = new Employee();
		e.setId(1001);
		e.setfName("Utkarsh");
		e.setlName("Gupta");
		e.setSalary(35000);
		
//		e.setSkills("Java",  "Python" , "sql");
		return e;
	}
	
	@Bean(name = "address")
	public Address getAddressBean() {
		Address address = new Address();
		address.setAreaName("Shiv_Colony");
		address.setCity("Pinjore");
		return address;	
	}
	
}
